from django.shortcuts import render

from django_filters.rest_framework import DjangoFilterBackend

from rest_framework import permissions , filters
from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.permissions import AllowAny, IsAdminUser, IsAuthenticated, IsAuthenticatedOrReadOnly

from todo.serializers import TodoSerializer 
from todo.models import Todo
from todo.pagination import CustomPageNumberPagination

from authentication.jwt import JWTAuthentication




class CreateTodoAPIView(CreateAPIView):
    
    serializer_class = TodoSerializer

    permission_classes = (IsAuthenticated,)

    authentication_classes = [JWTAuthentication,]

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

# تابع داخل کلاس بالا میگه که صاحب تودوعه کیه


class ListTodoAPIView(ListAPIView):

    serializer_class = TodoSerializer

    pagination_class=CustomPageNumberPagination

    permission_classes = (IsAuthenticated,)

    authentication_classes = [JWTAuthentication,]

    filter_backends = [DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter]

    filterset_fields = ['id','title','description','is_completed',]
    search_fields = ['id','title','description','is_completed',]
    ordering_fields = ['id','title','description','is_completed',]

    def get_queryset(self):
        return Todo.objects.filter(owner=self.request.user, is_deleted=False).order_by('-created_at')

# تابع داخل کلاس بالا بجای کواری ست نوشته شده که تودو های فقط اون شخصو میاره

"""
اگر میخواستی پست و گت رو باهم استفاده کنی
class Todos(ListCreateAPIView):
    serializer_class = TodoSerializer

    permission_classes = (IsAuthenticated,)

    authentication_classes = [JWTAuthentication,]

    def perform_create(self, serializer):
        return serializer.save(owner=self.request.user)

    def get_queryset(self):
        return Todo.objects.filter(owner=self.request.user, is_deleted=False).order_by('-created_at')

"""



class TodoDetailAPIView(RetrieveUpdateDestroyAPIView):

    serializer_class = TodoSerializer

    permission_classes = (IsAuthenticated,)

    authentication_classes = [JWTAuthentication,]

    lookup_field="id"

    def get_queryset(self):
        return Todo.objects.filter(owner=self.request.user).order_by('-created_at')

