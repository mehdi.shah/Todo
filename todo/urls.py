from django.urls import path

from todo.views import CreateTodoAPIView, ListTodoAPIView, TodoDetailAPIView


urlpatterns = [
    path('create', CreateTodoAPIView.as_view(), name='create-todo'),
    path('list/', ListTodoAPIView.as_view(), name='list-todo'),
    path('<int:id>', TodoDetailAPIView.as_view(), name='detail-todo'),
]

