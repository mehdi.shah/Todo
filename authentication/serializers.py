from rest_framework import serializers

from authentication.models import User



class RegisterSerializer(serializers.ModelSerializer):

    password = serializers.CharField(max_length=255, min_length=6, write_only=True)

    class Meta:
        model = User
        fields = ('username', 'email', 'password','first_name','last_name')


    def create(self, validated_data):
        return User.objects.create_user(**validated_data)



class LoginSerializer(serializers.ModelSerializer):

    password = serializers.CharField(max_length=255, min_length=6, write_only=True)

    class Meta:
        model = User
        fields = ('email','username','first_name','last_name','password','token','is_deleted')

        read_only_fields = ["token","username","first_name","last_name","is_deleted"]



class LogoutSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ('token',)

        read_only_fields = ["token",]