from django.shortcuts import render
from django.contrib.auth import authenticate

from rest_framework.generics import GenericAPIView
from rest_framework import response , status , permissions
from rest_framework.authentication import SessionAuthentication, BasicAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.views import APIView

from authentication.serializers import RegisterSerializer , LoginSerializer, LogoutSerializer
from authentication.jwt import JWTAuthentication




class RegisterAPIView(GenericAPIView):

    serializer_class = RegisterSerializer

    def post(self, request):
        serializer = self.serializer_class(data=request.data)


        if serializer.is_valid():
            serializer.save()

            return response.Response(serializer.data , status=status.HTTP_201_CREATED)      
            
        return response.Response(serializer.errors , status=status.HTTP_400_BAD_REQUEST)




class AuthUserAPIView(GenericAPIView):

    permission_classes = (permissions.IsAuthenticated,)

    authentication_classes = [JWTAuthentication,]

    def get(self, request):
        user = request.user
        serializer = RegisterSerializer(user)

        return response.Response({"user": serializer.data})






class LoginAPIView(GenericAPIView):

    serializer_class = LoginSerializer

    def post(self, request):
        email = request.data.get('email', None)
        password = request.data.get('password', None)
        
        user = authenticate(username=email, password=password)

        if user:
            serializer = self.serializer_class(user)

            return response.Response(serializer.data, status=status.HTTP_200_OK)

        return response.Response({"message":"invalid credentials, try again please."}, status=status.HTTP_401_UNAUTHORIZED)





